import pytest
from app import *

@pytest.fixture
def test_client():
    flask_app = app
    with flask_app.test_client() as test_client:
        yield test_client

@pytest.fixture
def open_img():
    file = open('test/000006.jpeg', 'rb')
    yield file

@pytest.fixture
def img_data(open_img):
    data={
        #'image_title' : 'Turlututu',
            #'description' : 'lalalala',
            'file' :(open_img, '000006.jpeg')}
    yield data